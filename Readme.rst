.. role:: bash(code)
   :language: bash

#. Install VirtualBox https://www.virtualbox.org/wiki/Downloads
#. Install Vagrant https://www.vagrantup.com/
#. Install Ansible http://docs.ansible.com/ansible/intro_installation.html
#. Run :bash:`vagrant up`
#. Open your browser http://localhost:8080/
#. Configure database, user: typo3, password: typo3, database: typo3
#. Create a new database e.g. typo3
